# Cortext Guix Channel

This is a channel for the [Guix](https://guix.gnu.org/) package manager. Refer to the [Guix manual](https://guix.gnu.org/manual/en/guix.html#Channels) for more details.

This channel contains package definitions of packages we use in cortext that are not available in Guix yet.

# Usage

Add the following to your `channels.scm` (default `$HOME/.config/guix/channels.scm`):

```scheme
(cons*
  (channel
    (name 'cortext)
    (url "https://gitlab.com/cortext/cortext-guix-channel.git")
    (branch "main"))
  ;; other channels
  %default-channels
)
```

Then run `guix pull` to update.

# License

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.