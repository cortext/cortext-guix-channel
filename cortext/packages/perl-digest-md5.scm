(define-module (cortext packages perl-digest-md5)
#:use-module (guix licenses)
#:use-module (guix packages)
#:use-module (guix build-system perl)
#:use-module (guix download))

(define-public perl-digest-md5
  (package
    (name "perl-digest-md5")
    (version "2.59")
    (source
     (origin
       (method url-fetch)
       (uri (string-append "mirror://cpan/authors/id/T/TO/TODDR/Digest-MD5-"
                           version ".tar.gz"))
       (sha256
        (base32 "1lkpmycxv0yh32wdkbp6mck7a95nfzcrqr9scr97v46mkc3vmv5m"))))
    (build-system perl-build-system)
    (home-page "https://metacpan.org/release/Digest-MD5")
    (synopsis "Perl interface to the MD-5 algorithm")
    (description "Perl interface to the MD-5 algorithm")
    (license perl-license)))

