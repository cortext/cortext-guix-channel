(define-module (cortext packages perl-text-unaccent-pureperl)
#:use-module (guix licenses)
#:use-module (guix packages)
#:use-module (guix build-system perl)
#:use-module (guix download)
#:use-module (gnu packages perl))

(define-public perl-text-unaccent-pureperl
  (package
    (name "perl-text-unaccent-pureperl")
    (version "0.05")
    (source
     (origin
       (method url-fetch)
       (uri (string-append
             "mirror://cpan/authors/id/P/PJ/PJACKLAM/Text-Unaccent-PurePerl-"
             version ".tar.gz"))
       (sha256
        (base32 "1wlb0sx2sryxvcilhg219qgsdymqsf913xnplgqvsr21ddr66bnj"))))
    (build-system perl-build-system)
    (home-page "https://metacpan.org/release/Text-Unaccent-PurePerl")
    (synopsis "remove accents from characters")
    (description "remove accents from characters")
    (license #f)))

