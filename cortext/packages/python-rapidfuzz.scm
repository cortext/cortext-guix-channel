(define-module (cortext packages python-rapidfuzz)
#:use-module ((guix licenses) #:prefix license:)
#:use-module (guix packages)
#:use-module (guix download)
#:use-module (guix build-system python)
#:use-module (guix build-system pyproject)
#:use-module (gnu packages check)
#:use-module (gnu packages python-build)
#:use-module (gnu packages python-xyz))

(define-public python-rapidfuzz
  (package
    (name "python-rapidfuzz")
    (version "3.6.1")
    (source
    (origin
      (method url-fetch)
      (uri (pypi-uri "rapidfuzz" version))
      (sha256
        (base32 "1rrx043zqqql31pcp92k61digibymp3l382gaxr4h8717kp0nrim"))))
    (build-system python-build-system)
    (native-inputs
    (list python-packaging
          python-pytest
          python-hypothesis
          python-scikit-build
          python-setuptools))
    (home-page "https://github.com/rapidfuzz/RapidFuzz")
    (synopsis "Rapid fuzzy string matching")
    (description "RapidFuzz is a fast string matching library for Python and
  C++, which is using the string similarity calculations from FuzzyWuzzy.")
    (license license:expat)))

