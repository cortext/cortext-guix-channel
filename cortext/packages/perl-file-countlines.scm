(define-module (cortext packages perl-file-countlines)
#:use-module (guix licenses)
#:use-module (guix packages)
#:use-module (guix build-system perl)
#:use-module (guix download)
#:use-module (gnu packages perl))

(define-public perl-file-countlines
  (package
    (name "perl-file-countlines")
    (version "0.0.3")
    (source
     (origin
       (method url-fetch)
       (uri (string-append
             "mirror://cpan/authors/id/M/MO/MORITZ/File-CountLines-v" version
             ".tar.gz"))
       (sha256
        (base32 "0qia72a5cwkhr0whfbzdxklinkvhnni78y6ld7jy84wngk77rnfg"))))
    (build-system perl-build-system)
    (native-inputs (list perl-module-build))
    (home-page "https://metacpan.org/release/File-CountLines")
    (synopsis "Efficiently count the number of line breaks in a file")
    (description "Efficiently count the number of line breaks in a file")
    (license perl-license)))

