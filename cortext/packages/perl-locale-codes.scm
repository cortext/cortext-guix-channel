(define-module (cortext packages perl-locale-codes)
#:use-module (guix licenses)
#:use-module (guix packages)
#:use-module (guix build-system perl)
#:use-module (guix download)
#:use-module (gnu packages perl-check))

(define-public perl-locale-codes
  (package
    (name "perl-locale-codes")
    (version "3.77")
    (source
     (origin
       (method url-fetch)
       (uri (string-append "mirror://cpan/authors/id/S/SB/SBECK/Locale-Codes-"
             version ".tar.gz"))
       (sha256
        (base32 "1h109ngfqym35ipbhfivz3nksd12xc99j34z0r917b0a02sv286g"))))
    (build-system perl-build-system)
    (native-inputs (list perl-test-inter))
    (home-page "https://metacpan.org/release/Locale-Codes")
    (synopsis "a distribution of modules to handle locale codes")
    (description "a distribution of modules to handle locale codes")
    (license perl-license)))
