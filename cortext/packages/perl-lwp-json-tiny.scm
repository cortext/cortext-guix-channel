(define-module (cortext packages perl-lwp-json-tiny)
#:use-module (guix licenses)
#:use-module (guix packages)
#:use-module (guix build-system perl)
#:use-module (guix download)
#:use-module (gnu packages web)
#:use-module (gnu packages perl)
#:use-module (gnu packages perl-check))

(define-public perl-lwp-json-tiny
  (package
    (name "perl-lwp-json-tiny")
    (version "0.014")
    (source
     (origin
       (method url-fetch)
       (uri (string-append
             "mirror://cpan/authors/id/S/SK/SKINGTON/LWP-JSON-Tiny-" version
             ".tar.gz"))
       (sha256
        (base32 "1b2480fiqr23556m0qydzp22zshyqvmlj4nw40x90r19kx9pmvg4"))))
    (build-system perl-build-system)
    (native-inputs (list perl-http-message perl-json-maybexs perl-libwww
                         perl-test-fatal))
    (propagated-inputs (list perl-http-message perl-json-maybexs
                             perl-libwww))
    (home-page "https://metacpan.org/release/LWP-JSON-Tiny")
    (synopsis "use JSON natively with LWP objects")
    (description "use JSON natively with LWP objects")
    (license perl-license)))

