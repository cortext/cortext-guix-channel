(define-module (cortext packages perl-unicode-casefold)
#:use-module (guix licenses)
#:use-module (guix packages)
#:use-module (guix build-system perl)
#:use-module (guix download)
#:use-module (gnu packages perl))

(define-public perl-unicode-casefold
  (package
    (name "perl-unicode-casefold")
    (version "1.01")
    (source
     (origin
       (method url-fetch)
       (uri (string-append
             "mirror://cpan/authors/id/A/AR/ARODLAND/Unicode-CaseFold-"
             version ".tar.gz"))
       (sha256
        (base32 "0k8rcdm87ffs6hawgm2kcybn9lrddl4hbj8a6fxvil7r10l232j1"))))
    (build-system perl-build-system)
    (native-inputs (list perl-module-build))
    (home-page "https://metacpan.org/release/Unicode-CaseFold")
    (synopsis "Unicode case-folding for case-insensitive lookups.")
    (description "Unicode case-folding for case-insensitive lookups.")
    (license perl-license)))

