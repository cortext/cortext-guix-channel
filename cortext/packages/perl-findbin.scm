(define-module (cortext packages perl-findbin)
#:use-module (guix licenses)
#:use-module (guix packages)
#:use-module (guix build-system perl)
#:use-module (guix download))

(define-public perl-findbin
(package
  (name "perl-findbin")
  (version "1.54")
  (source
   (origin
     (method url-fetch)
     (uri (string-append "mirror://cpan/authors/id/T/TO/TODDR/FindBin-"
                         version ".tar.gz"))
     (sha256
      (base32 "0pciybc1qpkaidnpjpnqm7bn4s8ffvifxplsjmvf77sjgsb35iva"))))
  (build-system perl-build-system)
  (home-page "https://metacpan.org/release/FindBin")
  (synopsis "Locate directory of original Perl script")
  (description "Perl package to Locate directory of original Perl script")
  (license perl-license)))