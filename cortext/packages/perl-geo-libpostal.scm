(define-module (cortext packages perl-geo-libpostal)
#:use-module (guix licenses)
#:use-module (guix packages)
#:use-module (guix build-system perl)
#:use-module (guix download)
#:use-module (gnu packages perl)
#:use-module (gnu packages perl-check)
#:use-module (cortext packages libpostal))

(define-public perl-geo-libpostal
  (package
    (name "perl-geo-libpostal")
    (version "0.08")
    (source
     (origin
       (method url-fetch)
       (uri (string-append
             "mirror://cpan/authors/id/D/DF/DFARRELL/Geo-libpostal-" version
             ".tar.gz"))
       (sha256
        (base32 "1mnxn0v1sj77ajcz3qbml2cv2zla2q4a8bwldxwafwmvhp0gr097"))))
    (build-system perl-build-system)
    (inputs (list libpostal))
    (propagated-inputs (list perl-test-fatal perl-const-fast perl-sub-exporter-progressive))
    (home-page "https://metacpan.org/release/Geo-libpostal")
    (synopsis "Perl bindings for libpostal")
    (description "Perl bindings for libpostal")
    (license bsd-2)))

