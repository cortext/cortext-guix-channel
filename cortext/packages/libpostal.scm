(define-module (cortext packages libpostal)
#:use-module ((guix licenses) #:prefix license:)
#:use-module (guix packages)
#:use-module (guix download)
#:use-module (guix git-download)
#:use-module (guix build-system gnu)
#:use-module (gnu packages admin)
#:use-module (gnu packages autotools)
#:use-module (gnu packages base)
#:use-module (gnu packages curl)
#:use-module (gnu packages networking)
#:use-module (gnu packages pkg-config))

(define-public libpostal
  (package
    (name "libpostal")
    (version "1.1")
    (source (origin 
              (method git-fetch)
              (uri (git-reference
                   (url "https://github.com/openvenues/libpostal/")
                   (commit "8f2066b1d30f4290adf59cacc429980f139b8545")))
              (file-name (git-file-name name version))
              (sha256
                (base32
                 "1d3819v9y3hcmrsyg86g74lwrfdb4gn8dl9b9hi0vy6ii3p88cwh"))))
    (build-system gnu-build-system)
    (arguments
      `(#:configure-flags (list (string-append "--datadir=" (assoc-ref %outputs "out") "/data"))
        #:phases
        (modify-phases %standard-phases
          (add-before 'configure 'addfiles
            (lambda* (#:key inputs outputs #:allow-other-keys)
              (let* ((out (assoc-ref outputs "out"))
                    (data (string-append out "/data/libpostal")))
                (mkdir-p data)
                (invoke "tar" "-xvzf" (assoc-ref inputs "libpostal_data.tar.gz") "-C" data)
                (invoke "tar" "-xvzf" (assoc-ref inputs "parser.tar.gz") "-C" data)
                (invoke "tar" "-xvzf" (assoc-ref inputs "language_classifier.tar.gz") "-C" data)
                (with-output-to-file (string-append data "/base_data_file_version")
                  (lambda _
                    (format #t "v1.0.0")))
                (with-output-to-file (string-append data "/language_classifier_model_file_version")
                  (lambda _
                    (format #t "v1.0.0")))
                (with-output-to-file (string-append data "/parser_model_file_version")
                  (lambda _
                    (format #t "v1.0.0")))
                (with-output-to-file (string-append data "/data_version")
                  (lambda _
                    (format #t "v1")))
                )))
          (add-before 'configure 'bootstrap
            (lambda* _ (invoke "./bootstrap.sh")))
          (add-after 'unpack 'patch-libpostal-data
            (lambda* (#:key outputs #:allow-other-keys)
              (let* ((out (assoc-ref outputs "out"))
                     (data (string-append out "/data/libpostal")))
                  (substitute* "src/libpostal_data.in"
                    ((".*mkdir.*") ""))
                  (substitute* "src/Makefile.am"
                    (("download all \\$\\(datadir\\)/libpostal") (string-append "download all " data)))))))))
    (inputs 
      `(("libpostal_data.tar.gz" ,(origin
          (method url-fetch)
          (uri "https://github.com/openvenues/libpostal/releases/download/v1.0.0/libpostal_data.tar.gz")
          (sha256 (base32 "18griy0hvyf2y9b3jkkpj1pjcd4i84rdxp7531pf99zkgdc51v6j"))))
        ("parser.tar.gz" ,(origin
          (method url-fetch)
          (uri "https://github.com/openvenues/libpostal/releases/download/v1.0.0/parser.tar.gz")
          (sha256 (base32 "091nl3sqx38gqr0rs2ra6lzff0q7wmszcs8jhv5swwaz16qfk53i"))))
        ("language_classifier.tar.gz" ,(origin
          (method url-fetch)
          (uri "https://github.com/openvenues/libpostal/releases/download/v1.0.0/language_classifier.tar.gz")
          (sha256 (base32 "0xx2d51mpqgi9kv65iww9nyrvasjd1k5ahjizvc2azkysfvfr9hn"))))))
    (native-inputs
      (list autoconf automake libtool pkg-config))
    (home-page "https://github.com/openvenues/libpostal")
    (synopsis "Libpostal: international street address NLP")
    (description "Libpostal is a C library for parsing/normalizing street addresses around the world using statistical NLP and open data.")
    (license license:expat)))

