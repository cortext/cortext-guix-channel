(define-module (cortext packages perl-text-csv-hashify)
#:use-module (guix licenses)
#:use-module (guix packages)
#:use-module (guix build-system perl)
#:use-module (guix download)
#:use-module (gnu packages perl))

(define-public perl-text-csv-hashify
  (package
    (name "perl-text-csv-hashify")
    (version "0.11")
    (source
     (origin
       (method url-fetch)
       (uri (string-append
             "mirror://cpan/authors/id/J/JK/JKEENAN/Text-CSV-Hashify-" version
             ".tar.gz"))
       (sha256
        (base32 "0xw24sij72zlb33qfqdsxz128dhkvs7bysz82yv032xd6rycrbx5"))))
    (build-system perl-build-system)
    (propagated-inputs (list perl-text-csv))
    (home-page "https://metacpan.org/release/Text-CSV-Hashify")
    (synopsis "Turn a CSV file into a Perl hash")
    (description "Turn a CSV file into a Perl hash")
    (license #f)))

