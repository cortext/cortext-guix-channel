(define-module (cortext packages perl-text-ngram)
#:use-module (guix licenses)
#:use-module (guix packages)
#:use-module (guix build-system perl)
#:use-module (guix download)
#:use-module (gnu packages perl)
#:use-module (cortext packages perl-unicode-casefold))

(define-public perl-text-ngram
  (package
    (name "perl-text-ngram")
    (version "0.15")
    (source
     (origin
       (method url-fetch)
       (uri (string-append
             "mirror://cpan/authors/id/A/AM/AMBS/Text/Text-Ngram-" version
             ".tar.gz"))
       (sha256
        (base32 "112ars7a6whvm3gynkgmrbms18nz65kczirwq915h517cjk1vvz2"))))
    (build-system perl-build-system)
    (propagated-inputs (list perl-unicode-casefold))
    (home-page "https://metacpan.org/release/Text-Ngram")
    (synopsis "Ngram analysis of text")
    (description "Ngram analysis of text")
    (license #f)))

